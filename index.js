const express = require('express')
const api = express()
const port = 8080
const filesFolder = './files/';
const fs = require('fs');
const path = require('path')
const bodyParser = require('body-parser');
const fsPromises = fs.promises;
api.use(bodyParser.json());
api.use(bodyParser.urlencoded({
  extended: true
}));

let dir = './files';

if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}


api.get('/api/files', function (req, res) {
  fs.readdir(filesFolder, (err, files) => {
    if(req.statusCode = 200){
      res.send(
        {
        "message": `Success`,
        "files": files
        }
      )
    }
    else if (req.statusCode = 400){
      res.send(
        {
          "message": "Client error"
        }
      )
    }
    else if (req.statusCode = 500){
      res.send(
        {
          "message": "Server error"
        }
      )
    }
    })
  }
  )

  api.get(`/api/files/:filename`, async (req, res) => {
    const fileName = req.params.filename;
    const regEx = /(?:\.([^.]+))?$/;
    const extension = regEx.exec(fileName)[1];

    if (extension === undefined) {
        res.status(400).send({ message: 'Please specify file extension' })
        return;
    }

    try {
        const data = await fsPromises.readFile(`${dir}/${fileName}`, 'utf8');
        const info = await fsPromises.stat(`${dir}/${fileName}`);
        res.status(200).send({
            message: 'Success',
            filename: fileName,
            content: data,
            extension: extension,
            uploadedDate: info.birthtime
        })
    } catch (err) {

        if (err.toString().includes(`No file with ${filename} filename found`)) {
            res.status(400).send({ message: 'No such file' })
            return;
        }

        res.status(500).send({ message: 'Server error' })
    }
});

    api.post('/api/files', function (req, res) {
      let filename = req.body.filename,
      content = req.body.content
      if(!content || !filename){
        res.status(400).send({
          "message": "Please specify 'content' parameter"
        })
      }
      else if(res.statusCode = 200){
        fs.writeFile(`./files/${filename}`,content, function (err) {
          if (err) throw err;
          res.send({
            "message": "File created successfully"
          });
        })
      }
      else if(res.statusCode = 500){
        res.send({
          "message": "Server error"
        })
      }
    })

api.listen(port, () => {
})
  
